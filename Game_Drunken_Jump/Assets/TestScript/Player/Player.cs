﻿using System;
using Manager;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Character
{
    public class Player : MonoBehaviour,IDamageable
    {
        [SerializeField] int currentHp;
        [SerializeField] int maxHp;
        public HealthBar healthBar;
        
        void Start()
        {
            currentHp = maxHp;
            healthBar.SetHp(currentHp,maxHp);
        }
        
        public void TakeHit(int damage)
        {
            currentHp -= damage;
            healthBar.SetHp(currentHp,maxHp);
            if (currentHp <= 0)
            {
                Dead();
            }
            
        }

        private void Dead()
        {
            GameManagement.Instance.OnPlayerDead();
            gameObject.SetActive(false);
            SoundManager.Instance.Play(SoundManager.Sound.PlayerDie);
        }
        
        private void OnCollisionEnter2D(Collision2D other)
        {
            if (other.collider.CompareTag("DestroyObj"))
            {
                Dead();
            }
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.CompareTag("Gate"))
            {
                SceneManager.LoadScene("GameLv2");
            }
        }
    }
}