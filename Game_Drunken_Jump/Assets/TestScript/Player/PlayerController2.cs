﻿using UnityEngine;

namespace Character
{
    public class PlayerController2 : MonoBehaviour
    {
        [Header("Movement")] 
        [SerializeField] float moveSpeed = 10f;
        [SerializeField] float airMoveSpeed = 10f;
        private float xDirectionalInput;
        private bool facingRight = true;
        private bool isMoving;

        [Header("Jumping")] 
        [SerializeField] float jumpForce = 16f;
        [SerializeField] LayerMask groundLayer;
        [SerializeField] Transform groundCheckPoint;
        [SerializeField] Vector2 groundCheckSize;
        private bool isGrounded;
        private bool canJump;

        [Header("WallSliding")] [SerializeField]
        float wallSlideSpeed;

        [SerializeField] LayerMask wallLayer;
        [SerializeField] Transform wallCheckPoint;
        [SerializeField] Vector2 wallCheckSize;
        private bool isTouchingWall;
        private bool isWallSliding;

        [Header("WallJumping")] 
        [SerializeField] float walljumpforce;

        [SerializeField] Vector2 walljumpAngle;
        [SerializeField] float walljumpDirection = -1;

        [Header("Parachulte")] 
        public GameObject parachute;
        [SerializeField] private int drag;

        [Header("Other")] [SerializeField] Animator animte;
        private Rigidbody2D rb;
        
        private void Start()
        {
            rb = GetComponent<Rigidbody2D>();
            walljumpAngle.Normalize();
            if (canJump == false)
            {
                rb.drag = drag;
            }
        }

        private void Update()
        {
            Inputs();
            CheckWorld();
            AnimationControl();
            if (isGrounded)
            {
                Destroy(parachute);
                rb.drag = 0;
            }
        }

        private void FixedUpdate()
        {
            Movement();
            Jump();
            WallSlide();
            WallJump();
        }

        void Inputs()
        {
            xDirectionalInput = Input.GetAxis("Horizontal");
            if (Input.GetKeyDown(KeyCode.Space))
            {
                canJump = true;
            }
        }

        void CheckWorld()
        {
            isGrounded = Physics2D.OverlapBox(groundCheckPoint.position, groundCheckSize, 0, groundLayer);
            isTouchingWall = Physics2D.OverlapBox(wallCheckPoint.position, wallCheckSize, 0, wallLayer);
        }

        void Movement()
        {
            isMoving = xDirectionalInput != 0;
            
            if (isGrounded)
            {
                rb.velocity = new Vector2(xDirectionalInput * moveSpeed, rb.velocity.y);
            }
            else if (!isGrounded && (!isWallSliding || !isTouchingWall) && xDirectionalInput != 0)
            {
                rb.AddForce(new Vector2(airMoveSpeed * xDirectionalInput, 0));
                if (Mathf.Abs(rb.velocity.x) > moveSpeed)
                {
                    rb.velocity = new Vector2(xDirectionalInput * moveSpeed, rb.velocity.y);
                }
            }
            
            if (xDirectionalInput < 0 && facingRight)
            {
                Flip();
            }
            else if (xDirectionalInput > 0 && !facingRight)
            {
                Flip();
            }
        }

        void Flip()
        {
            if (!isWallSliding)
            {
                walljumpDirection *= -1;
                facingRight = !facingRight;
                transform.Rotate(0, 180, 0);
            }

        }

        void Jump()
        {
            if (canJump && isGrounded)
            {
                rb.velocity = new Vector2(rb.velocity.x, jumpForce);
                canJump = false;
            }

        }

        void WallSlide()
        {
            if (isTouchingWall && !isGrounded && rb.velocity.y < 0)
            {
                isWallSliding = true;
            }
            else
            {
                isWallSliding = false;
            }

            if (isWallSliding)
            {
                rb.velocity = new Vector2(rb.velocity.x, -wallSlideSpeed);
            }
        }

        void WallJump()
        {
            if ((isWallSliding) && canJump)
            {
                rb.AddForce(
                    new Vector2(walljumpforce * walljumpAngle.x * walljumpDirection, walljumpforce * walljumpAngle.y),
                    ForceMode2D.Impulse);
                Flip();
                canJump = false;
            }
        }

        void AnimationControl()
        {
            animte.SetBool("isMoving", isMoving);
            animte.SetBool("isGrounded", isGrounded);
            animte.SetBool("isSliding", isTouchingWall);
        }

        private void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.blue;
            Gizmos.DrawCube(groundCheckPoint.position, groundCheckSize);
            Gizmos.color = Color.red;
            Gizmos.DrawCube(wallCheckPoint.position, wallCheckSize);
        }
    }
}