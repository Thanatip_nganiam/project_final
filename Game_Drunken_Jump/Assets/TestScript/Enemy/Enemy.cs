﻿using System;
using UnityEngine;
using Manager;

namespace Character
{
    public class Enemy : MonoBehaviour,IDamageable
    {
        [SerializeField] float addForce;
        [SerializeField] int currentHp;
        [SerializeField] int maxHp;
        [SerializeField] Transform shootPoint;
        [SerializeField] GameObject bullet;
        [SerializeField] float fireRate = 1.0f;
        [SerializeField] float nextFire = 1.0f;
        public HealthBar healthBar;

        void Start()
        {
            currentHp = maxHp;
            healthBar.SetHp(currentHp,maxHp);
        }

        void Update()
        {
            if (Time.time > nextFire)
            {
                nextFire = Time.time + fireRate;
                {
                    Fire();
                }
            }
        }
        
        public void TakeHit(int damage)
        {
            currentHp -= damage;
            healthBar.SetHp(currentHp,maxHp);
            if (currentHp <= 0)
            {
                Dead();
            }
        }

        private void Dead()
        {
            if (CompareTag("Boss"))
            {
                GameManagement.Instance.OnBossDead();
                ScoreManager.Instance.SetScore(999999);
            }
            if (CompareTag("Enemy"))
            {
                ScoreManager.Instance.SetScore(1);
            }
            gameObject.SetActive(false);
            SoundManager.Instance.Play(SoundManager.Sound.EnemyDie);
        }


        private void Fire()
        {
            var newBullet = Instantiate(bullet, shootPoint.position, shootPoint.rotation);
            newBullet.GetComponent<Rigidbody2D>().velocity = transform.right * addForce;
            SoundManager.Instance.Play(SoundManager.Sound.EnemyFire);
        }
    }
}