﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Enemy
{
    public class EnemyController : MonoBehaviour
    {
        [SerializeField] float speed;
        [SerializeField] Transform downDetection;
        private bool _movingRight = true;
        
        void Update()
        {
            EnemyMovement();
        }
        private void EnemyMovement()
        {
            transform.Translate(Vector2.right * speed * Time.deltaTime);
            RaycastHit2D groundInfo = Physics2D.Raycast(downDetection.position, Vector2.down, 2f);
            if (groundInfo.collider == false)
            {
                if (_movingRight == true)
                {
                    transform.eulerAngles = new Vector3(0, -180, 0);
                    _movingRight = false;
                }
                else
                {
                    transform.eulerAngles = new Vector3(0, 0, 0);
                    _movingRight = true;
                }
            }
        }
    }
}