﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Character
{
    public class TerretShoot : MonoBehaviour
    {
        [SerializeField] float addForce;
        [SerializeField] Transform firePoint;
        [SerializeField] GameObject bulletPrefab;
        [SerializeField] float fireRate = 1.0f;
        [SerializeField] float nextFire = 1.0f;

        void Update()
        {
            if (Time.time > nextFire)
            {  
                nextFire = Time.time + fireRate;
                {
                    Shoot();
                }
            }
        }
        void Shoot ()
        {
            GameObject newBullet = Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);
            newBullet.GetComponent<Rigidbody2D>().velocity = transform.right * addForce;
        }
    }
}
