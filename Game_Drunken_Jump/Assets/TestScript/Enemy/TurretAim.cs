﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretAim : MonoBehaviour
{
    [SerializeField] Transform target;
    [SerializeField] float enemyAimSpeed = 5.0f; 
    Quaternion _newRotation;
    private float _orientTransform;
    private float _orientTarget;

    void Update () 
    {
        _orientTransform = transform.position.x;
        _orientTarget = target.position.x;
       
        if (target == null)
            return;
        
        if (_orientTransform > _orientTarget) 
        {
            _newRotation = Quaternion.LookRotation (transform.position - target.position, -Vector3.up);
        }
        else 
        {
            _newRotation = Quaternion.LookRotation (transform.position - target.position,Vector3.up);
        }
 
        _newRotation.x = 0.0f;
        _newRotation.y = 0.0f;
 
        transform.rotation = Quaternion.Lerp (transform.rotation,_newRotation,Time.deltaTime * enemyAimSpeed);
    }
}
