﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Manager
{
    public class ScoreManager : Singleton<ScoreManager>
    {
        [SerializeField] public TextMeshProUGUI scoreText;
        private int playerScore;
        
        public void SetScore(int score)
        {
            playerScore += score;
            scoreText.text = $"Score : {playerScore}";
        }
    }
}
