﻿using System;
using Character;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

namespace Manager
{
    public class GameManagement : Singleton<GameManagement>
    {
        [SerializeField] private Button restart;
        [SerializeField] private Button exit;
        [SerializeField] private RectTransform dialog;
        [SerializeField] private RectTransform endDialog;

        private void Start()
        {
            dialog.gameObject.SetActive(false);
            endDialog.gameObject.SetActive(false);
        }
        
        private void OnRestartClicked()
        {
            SceneManager.LoadScene(1);
            dialog.gameObject.SetActive(false);
            endDialog.gameObject.SetActive(false);
        }
        
        public void OnPlayerDead()
        {
            Restart();
        }

        private void OnExitClicked()
        {
            SceneManager.LoadScene("Menu");
        }
        public void OnBossDead()
        {
            DestroyRemainingEnemy();
            endDialog.gameObject.SetActive(true);
            exit.onClick.AddListener(OnExitClicked);
        }

        private void Restart()
        {
            DestroyRemainingEnemy();
            dialog.gameObject.SetActive(true);
            restart.onClick.AddListener(OnRestartClicked);
            exit.onClick.AddListener(OnExitClicked);
        }

        private void DestroyRemainingEnemy()
        {
            var remainingEnemy = GameObject.FindGameObjectsWithTag("Enemy");
            foreach (var enemy in remainingEnemy)
            {
                Destroy(enemy);
            }
            
            var boss = GameObject.FindGameObjectsWithTag("Boss");
            foreach (var enemy in boss)
            {
                Destroy(enemy);
            }
            
            var remainingPlayer = GameObject.FindGameObjectsWithTag("Player");
            foreach (var player in remainingPlayer)
            {
                Destroy(player);
            }
        }
    }
}
