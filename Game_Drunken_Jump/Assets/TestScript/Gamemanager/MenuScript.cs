﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

namespace Manager
{
    public class MenuScript : MonoBehaviour
    {
        [SerializeField] private Button startGame;
        [SerializeField] private Button exit;
        [SerializeField] private RectTransform dialog;

        private void Awake()
        {
            startGame.onClick.AddListener(OnstartButtonclicked);
            exit.onClick.AddListener(OnExitClicked);
        }

        private void OnstartButtonclicked()
        {
            SceneManager.LoadScene("Game");
            dialog.gameObject.SetActive(false);
        }

        private void OnExitClicked()
        {
            Application.Quit();
        }


    }

}
