﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Manager
{
    public class MovingPlatform : MonoBehaviour
    {
        [SerializeField] List<Transform> points;
        [SerializeField] Transform platform;
        private int goalPoint = 0;
        [SerializeField] public float moveSpeed = 2;
        
        private void Update()
        {
            MoveToNextPoint();
        }
        void MoveToNextPoint()
        {
            if (platform == null)
                return;
            platform.position = Vector2.MoveTowards(platform.position, points[goalPoint].position, Time.deltaTime * moveSpeed);
            if (Vector2.Distance(platform.position, points[goalPoint].position) < 0.1f)
            {
                if (goalPoint == points.Count - 1)
                    goalPoint = 0;
                else
                    goalPoint++;
            }
        }
    }
}
