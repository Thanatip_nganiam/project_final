﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Manager
{
    public class HealthBar : MonoBehaviour
    {
        [SerializeField] Slider slider;
        [SerializeField] Color low;
        [SerializeField] Color high;
        [SerializeField] Vector3 offset;

        public void SetHp(float hp, float maxHp)
        {
            slider.gameObject.SetActive(hp < maxHp);
            slider.value = hp;
            slider.maxValue = maxHp;

            slider.fillRect.GetComponent<Image>().color = Color.Lerp(low, high, slider.normalizedValue);
        }
        void Update()
        {
            if (!(Camera.main is null))
                slider.transform.position = Camera.main.WorldToScreenPoint(transform.parent.position + offset);
        }
    }
}
