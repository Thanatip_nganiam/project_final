﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Character
{
    public interface IDamageable
    {
        void TakeHit(int damage);
    }
}
