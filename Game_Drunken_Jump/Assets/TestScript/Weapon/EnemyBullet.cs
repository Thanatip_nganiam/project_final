using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Character
{
    public class EnemyBullet : MonoBehaviour
    {
        Rigidbody2D rb;
        private bool hasHit;
        [SerializeField] public int damage;
        [SerializeField] private float interval;

        void Start()
        {
            rb = GetComponent<Rigidbody2D>();
        }

        void Update()
        {
            BulletMove();
        }

        private void OnCollisionEnter2D(Collision2D other)
        {
            hasHit = true;
            rb.velocity = Vector2.zero;
            rb.isKinematic = true;
            if (other.collider.CompareTag("Player"))
            { 
                var target = other.gameObject.GetComponent<IDamageable>();
                target?.TakeHit(damage);
            }
            Destroy(gameObject);
        }

        private void DestroyBullet2D(GameObject bullet)
        {
            if (interval > 0)
            {
                interval -= Time.deltaTime;
            }
            else
            {
                enabled = false;
                Destroy(bullet);
            }
        }
        private void BulletMove()
        {
            float angle = Mathf.Atan2(rb.velocity.y, rb.velocity.x) * Mathf.Rad2Deg;
            transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
            DestroyBullet2D(gameObject);
        }
        
    }
}