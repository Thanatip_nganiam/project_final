using System;
using System.Collections;
using System.Collections.Generic;
using Manager;
using UnityEngine;

namespace Character
{
    public class Weapon : MonoBehaviour
    {
        public GameObject bullet;
        [SerializeField] float addForce;
        public Transform shootPoint;

        public GameObject point;
        GameObject[] points;
        [SerializeField] int numberOfPoints;
        [SerializeField] float spaceBetweenPoints;
        Vector2 direction;

        private void Start()
        {
            points = new GameObject[numberOfPoints];
            for (int i = 0; i < numberOfPoints; i++)
            {
                points[i] = Instantiate(point, shootPoint.position, Quaternion.identity);
            }
        }

        void Update()
        {
            Vector2 weaponPosition = transform.position;
            Vector2 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            direction = mousePosition - weaponPosition;
            transform.right = direction;
            if (Input.GetMouseButtonDown(0))
            {
                Shoot();
            }
            
            for (int i = 0; i < numberOfPoints; i++)
            {
                points[i].transform.position = PointPos(i * spaceBetweenPoints);
            }
        }

        void Shoot()
        {
            GameObject newBullet = Instantiate(bullet, shootPoint.position, shootPoint.rotation);
            newBullet.GetComponent<Rigidbody2D>().velocity = transform.right * addForce;
            SoundManager.Instance.Play(SoundManager.Sound.PlayerFire);
        }

        Vector2 PointPos(float time)
        {
            Vector2 currentPointPos = (Vector2)shootPoint.position + (direction.normalized * addForce * time) + 0.5f * Physics2D.gravity * (time * time);   
            return currentPointPos;
        }
    }
}
