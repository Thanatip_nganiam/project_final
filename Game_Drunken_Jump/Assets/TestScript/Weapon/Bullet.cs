using System;
using System.Collections;
using System.Collections.Generic;
using Manager;
using UnityEngine;

namespace Character
{
    public class Bullet : MonoBehaviour
    {
        [SerializeField] public int damage;
        [SerializeField] private float interval;
        Rigidbody2D rb;
        private bool hasHit;

        void Start()
        {
            rb = GetComponent<Rigidbody2D>();
        }

        void Update()
        {
            BulletMove();
        }

        private void OnCollisionEnter2D(Collision2D other)
        {
            var target = other.gameObject.GetComponent<IDamageable>();
            hasHit = true;
            rb.velocity = Vector2.zero;
            rb.isKinematic = true;
            target?.TakeHit(damage);
            Destroy(gameObject);
        }

        private void DestroyBullet2D(GameObject bullet)
        {
            if (interval > 0)
            {
                interval -= Time.deltaTime;
            }
            else
            {
                enabled = false;
                Destroy(bullet);
            }
        }
        private void BulletMove()
        {
            float angle = Mathf.Atan2(rb.velocity.y, rb.velocity.x) * Mathf.Rad2Deg;
            transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
            DestroyBullet2D(gameObject);
        }
        
    }
}